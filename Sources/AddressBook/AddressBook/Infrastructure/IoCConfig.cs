﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using DataAccessLayer;

namespace AddressBook.Infrastructure
{
    public static class IoCConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            
            builder.RegisterType<AddressBookEntities>().AsSelf().InstancePerRequest();

            builder.RegisterControllers(typeof(MvcApplication).Assembly)
               .InstancePerRequest();

            builder.RegisterModule<AutofacWebTypesModule>();
            
            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}