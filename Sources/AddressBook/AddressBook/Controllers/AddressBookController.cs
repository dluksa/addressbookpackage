﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using DataAccessLayer;

namespace AddressBook.Controllers
{
    [Authorize]
    public class AddressBookController : Controller
    {
        private readonly AddressBookEntities _db;

        public AddressBookController(AddressBookEntities db)
        {
            _db = db;
        }

        // GET: AddressBook
        public ActionResult Index(string search)
        {
            var contacts = _db.Contact.AsQueryable();

            if (!string.IsNullOrEmpty(search))
            {
                contacts =
                    contacts.Where(x => 
                        x.Address.Contains(search) ||
                        x.FirstName.Contains(search) || x.LastName.Contains(search) ||
                        string.Concat(x.FirstName, " ", x.LastName).Contains(search) ||
                        x.EmailAddress.Any(e => e.Email.Contains(search)) ||
                        x.PhoneNumber.Any(p => p.PhoneNumber1.Contains(search))
                    );
            }

            return View(contacts.Take(50).ToList());
        }

        // GET: AddressBook/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = _db.Contact.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // GET: AddressBook/Create
        public ActionResult Create()
        {
            var model = new Contact();
            return View(model);
        }

        // POST: AddressBook/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind] Contact contact)
        {
            if (ModelState.IsValid)
            {
                _db.Contact.Add(contact);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contact);
        }

        // GET: AddressBook/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = _db.Contact.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: AddressBook/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind] Contact contact)
        {
            if (ModelState.IsValid)
            {
                var contactNumbers =
                    _db.PhoneNumber.Where(x => x.ContactId == contact.ContactId);

                var currentPhoneNumberIds = contact.PhoneNumber.Select(c => c.PhoneNumberId);

                var deletedNumbers =
                    contactNumbers.Where(x => !currentPhoneNumberIds.Contains(x.PhoneNumberId)).ToList();

                _db.PhoneNumber.RemoveRange(deletedNumbers);

                foreach (var phoneNumber in contact.PhoneNumber)
                {
                    
                    _db.PhoneNumber.AddOrUpdate(phoneNumber);
                }
                contact.PhoneNumber = new HashSet<PhoneNumber>();

                var contactEmails =
                    _db.EmailAddress.Where(x => x.ContactId == contact.ContactId);

                var currentEmailIds = contact.EmailAddress.Select(c => c.EmailAddressId).ToArray();

                var deletedEmails =
                    contactEmails.Where(x => !currentEmailIds.Contains(x.EmailAddressId));

                _db.EmailAddress.RemoveRange(deletedEmails);

                foreach (var email in contact.EmailAddress)
                {

                    _db.EmailAddress.AddOrUpdate(email);
                }
                contact.EmailAddress = new HashSet<EmailAddress>();


                _db.Entry(contact).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Details", new { id = contact.ContactId});
            }
            return View(contact);
        }

        // GET: AddressBook/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = _db.Contact.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: AddressBook/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact contact = _db.Contact.Find(id);
            _db.Contact.Remove(contact);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        public PartialViewResult CreatePhoneNumber(int? contactId)
        {
            var model = new PhoneNumber();
            if (contactId.HasValue)
                model.ContactId = contactId.Value;

            return PartialView("~/Views/Shared/EditorTemplates/PhoneNumber.cshtml", model);
        }

        public PartialViewResult CreateEmailAddress(int? contactId)
        {
            var model = new EmailAddress();
            if (contactId.HasValue)
                model.ContactId = contactId.Value;

            return PartialView("~/Views/Shared/EditorTemplates/EmailAddress.cshtml", model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
