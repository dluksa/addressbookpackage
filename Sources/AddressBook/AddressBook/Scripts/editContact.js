﻿function onItemAddSuccess() {
    $("form").removeData("validator");
    $("form").removeData("unobtrusiveValidation");
    $.validator.unobtrusive.parse("form");
}
function deleteItem(guid) {
    $("#" + guid).remove();
}