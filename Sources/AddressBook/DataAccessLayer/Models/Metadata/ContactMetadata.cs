﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models.Metadata
{
    class ContactMetadata
    {
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        public string Address { get; set; }
    }
}