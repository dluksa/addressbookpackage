﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models.Metadata
{
    class PhoneNumberMetadata
    {
        [Required]
        [RegularExpression("^\\+?[0-9]{1,3}\\s?[0-9]{4,14}(?:x.+)?$", ErrorMessage = "Phone number is invalid.")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber1 { get; set; }
    }
}
