﻿CREATE TABLE [adr].[EmailAddress]
(
	[EmailAddressId] INT NOT NULL PRIMARY KEY IDENTITY,
	[Email] NVARCHAR(320) NOT NULL,
	[ContactId] INT NOT NULL, 
    CONSTRAINT [FK_EmailAddress_Contact] FOREIGN KEY ([ContactId]) REFERENCES [adr].[Contact]([ContactId]) ON DELETE CASCADE
)
