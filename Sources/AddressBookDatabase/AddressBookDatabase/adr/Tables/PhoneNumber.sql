﻿CREATE TABLE [adr].[PhoneNumber]
(
	[PhoneNumberId] INT NOT NULL PRIMARY KEY IDENTITY,
	[PhoneNumber] VARCHAR(100) NOT NULL,
	[ContactId] INT NOT NULL,
	CONSTRAINT [FK_PhoneNumber_Contact] FOREIGN KEY ([ContactId]) REFERENCES [adr].[Contact]([ContactId]) ON DELETE CASCADE
)
